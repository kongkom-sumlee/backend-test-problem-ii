# How to run project in local environment (upload file api)

##### 1. open project file

- open terminal in project file

##### 2. install package json

- ### `npm install`

##### 3. run project

- ### `npm run dev`

##### 4. call api with postman 

- curl --location 'http://localhost:3000/problem/v1/two' \
--header 'Authorization: DEVCREW-BACKEND-TEST' \
--form 'file=@"/path/to/file"'

- curl --location 'http://localhost:3000/problem/v1/two' \
--header 'Authorization: DEVCREW-BACKEND-TEST' \
--form 'file=@"/Users/Tang/Desktop/Test.docx"' --> [ex. '/Users/Tang/Desktop/Test.docx' your file path]

##### credit by Kongkom Sumlee
