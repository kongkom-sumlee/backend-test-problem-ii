import express, { Express } from 'express'
import dotenv from 'dotenv'
import problemController from './routes/ProblemController'

dotenv.config()

const app: Express = express()
const port: string | 3000 = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/problem', problemController)

app.listen(port, () => {
    console.log(`Server is running at PORT ${port}`)
})
