export default interface GetFileDataResponse {
    fileName: string,
    size: string,
    extension: string
}
