import validator from '../utilities/Validator'
import helper from './FileManagementServiceHelper'

class FileManagementService {
    public async uploadFile(file: any) {
        validator.validateFile(file)

        const fileData = helper.getFileData(file)
        const response = helper.mapFileResponse(fileData)

        return response
    }
}

export default new FileManagementService()
