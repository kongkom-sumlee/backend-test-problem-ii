import GetFileDataResponse from '../interfaces/response/GetFileDataResponse.interface'

class FileManagementServiceHepler {
    public getFileData(file: any): GetFileDataResponse {
        const fileName = file.originalname
        const size = `${(file.size / (1024 * 1024))}MB`
        const extension = fileName.split('.')[1]

        return { fileName, size, extension}
    }

    public mapFileResponse(fileData: any): GetFileDataResponse {
        return {
            fileName: fileData.fileName,
            size: fileData.size,
            extension: fileData.extension
        }
    }
}

export default new FileManagementServiceHepler()
