function generateErrorResponse(error: any) {
    return {
      error: error.error
    }
}

export { generateErrorResponse }
