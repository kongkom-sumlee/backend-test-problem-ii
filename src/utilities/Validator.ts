import CustomError from './CustomError'

function validateFile(requestFile: any) {
    if (!requestFile) throw new CustomError(422, 'Please select a file')
}

export default {
    validateFile
}
