import express, { Request, Response, Router } from 'express'
import CustomError from '../utilities/CustomError'
import { generateErrorResponse } from '../utilities/ResponseHandler'
import fileManagementService from '../services/FileManagementService'
import multer from 'multer'

const router: Router = express.Router()
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

router.post('/v1/two', upload.single('file'), async(req: Request, res: Response) => {
    try {
        const response = await fileManagementService.uploadFile(req.file)

        res.status(200).json(response)
    } catch (err) {
        if (err instanceof CustomError) {
          const errorResponse = generateErrorResponse(err)
          res.status(err.statusCode).json(errorResponse)
        } else {
          res.status(500).json(generateErrorResponse('Internal server error'))
        }
    }
})

export default router
